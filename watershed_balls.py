import cv2
import numpy as np
from random import shuffle
import matplotlib.pyplot as plt

cam = cv2.VideoCapture(0)
cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)
cv2.namedWindow("Bin camera", cv2.WINDOW_FREERATIO)

hsv_colors = [
    ((50, 100, 150), (90, 255, 255), "G"),   # G
    ((25, 50, 150), (33, 255, 255), "Y"),   # Y
    ((8, 65, 150), (22, 255, 255), "O")    # O
]

color_idx = 0
color, color_name = hsv_colors[color_idx][:-1], hsv_colors[color_idx][-1] 

print(f"Current color: {color_name}")

while cam.isOpened():
    _, img = cam.read()
    img = cv2.flip(img, 1)
    img = cv2.GaussianBlur(img, (11, 11), sigmaX=0)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    mask = cv2.inRange(hsv_img, color[0], color[-1])
    mask = cv2.erode(mask, None, iterations=3)
    mask = cv2.dilate(mask, None, iterations=3)

    contours, hierarchy = cv2.findContours(mask, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    
    dist = cv2.distanceTransform(mask, cv2.DIST_L2, 5)
    reg, fg = cv2.threshold(dist, dist.max() * 0.2, 255, 0)
    fg = np.uint8(fg)

    confuse = cv2.subtract(mask, fg)

    ret, markers = cv2.connectedComponents(fg)
    markers += 1
    markers[confuse == 255] = 0

    wmarkers = cv2.watershed(img, markers.copy())

    contours, hierarchy = cv2.findContours(wmarkers.copy(), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)

    cnt = 0
    for i in range(len(contours)):
        if hierarchy[0, i, 3] == -1 \
            and hierarchy[0, i, 0] != -1 \
            and cv2.contourArea(contours[i]) > 200.0:

            cnt += 1
            cv2.drawContours(img, contours, i, (0, 0, 255), 5)

    if cnt > 0:
        cnt //= 2
        print(f"Balls number: {cnt}")
    
    key = cv2.waitKey(1)
    if key == ord('q'):
        break

    cv2.imshow("Camera", img)
    cv2.imshow("Bin camera", np.uint8(wmarkers))

cam.release()
cv2.destroyAllWindows()
